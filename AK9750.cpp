#include "AK9750.h"
#include "AK9750_reg.h"


AK9750::AK9750() {
}

void AK9750::init(I2C *conn, SlaveAddress addr) {
    slaveAddress = addr;
    connection = conn;
}

AK9750::Status AK9750::checkConnection() {
    Status status = AK9750::SUCCESS;
    
    // Gets the WIA register value.
    char buf[2];
    if ((status=AK9750::read(AK9750_REG_ADDR_WIA1, buf, 2)) != SUCCESS) {
        return status;
    }

    // Checks the obtained value equals to the supposed value.
    if ( (buf[0] != AK9750_REG_VALUE_WIA1) ||  (buf[1] != AK9750_REG_VALUE_WIA2) ) {
        return AK9750::ERROR;
    }
    
    return status;
}

AK9750::Status AK9750::read(char registerAddress, char *buf, int length) {
    // Writes the first register address
    if (connection->write((slaveAddress << 1), &registerAddress, AK9750_LEN_ONE_BYTE) != 0) {
        // I2C write failed.
        return AK9750::ERROR_I2C_WRITE;
    }
    
    // Reads register data
    if (connection->read((slaveAddress << 1), buf, length) != 0) {
        // I2C read failed.
        return AK9750::ERROR_I2C_READ;
    }
    
    return AK9750::SUCCESS;
}

AK9750::Status AK9750::write(char registerAddress, const char *buf, int length) {
    char data[AK9750_LEN_BUF_MAX];

    // Creates data to be sent.
    data[0] = registerAddress;    
    for (int i=0; i < length; i++) {
        data[1+i] = buf[i];
    }
    
    // Writes data. 
    if (connection->write((slaveAddress << 1), data, length + 1) != 0) {
        // I2C write failed.
        return AK9750::ERROR_I2C_WRITE;
    }

    return AK9750::SUCCESS;
}

AK9750::Status AK9750::getInterruptEnable(InterruptStatus *intStatus){
    Status status;
    
    char buf = 0;
    if((status=read(AK9750_REG_ADDR_EINTEN, &buf, AK9750_LEN_ONE_BYTE)) != SUCCESS) {
        return status;
    }
    
    intStatus->ir13h = ((buf & AK9750_INT_STATUS_FLAG_IR13H) > 0) ? true : false;
    intStatus->ir13l = ((buf & AK9750_INT_STATUS_FLAG_IR13L) > 0) ? true : false;
    intStatus->ir24h = ((buf & AK9750_INT_STATUS_FLAG_IR24H) > 0) ? true : false;
    intStatus->ir24l = ((buf & AK9750_INT_STATUS_FLAG_IR24L) > 0) ? true : false;
    intStatus->drdy = ((buf & AK9750_INT_STATUS_FLAG_DRDY) > 0) ? true : false;
    
    return SUCCESS;
}

AK9750::Status AK9750::getInterruptEnableFromEEPROM(InterruptStatus *intStatus){
    Status status;
    
    if (setEepromReadMode() != SUCCESS) return ERROR;    

    char buf = 0;
    if((status=read(AK9750_EEPROM_ADDR_EINTEN, &buf, AK9750_LEN_ONE_BYTE)) != SUCCESS) {
        return status;
    }
    
    intStatus->ir13h = ((buf & AK9750_INT_STATUS_FLAG_IR13H) > 0) ? true : false;
    intStatus->ir13l = ((buf & AK9750_INT_STATUS_FLAG_IR13L) > 0) ? true : false;
    intStatus->ir24h = ((buf & AK9750_INT_STATUS_FLAG_IR24H) > 0) ? true : false;
    intStatus->ir24l = ((buf & AK9750_INT_STATUS_FLAG_IR24L) > 0) ? true : false;
    intStatus->drdy = ((buf & AK9750_INT_STATUS_FLAG_DRDY) > 0) ? true : false;
    
    return SUCCESS;
}

AK9750::Status AK9750::setInterruptEnable(const AK9750::InterruptStatus *intStatus) {
    char buf = 0;
    
    buf += intStatus->ir13h ? AK9750_INT_STATUS_FLAG_IR13H : 0;
    buf += intStatus->ir13l ? AK9750_INT_STATUS_FLAG_IR13L : 0;
    buf += intStatus->ir24h ? AK9750_INT_STATUS_FLAG_IR24H : 0;
    buf += intStatus->ir24l ? AK9750_INT_STATUS_FLAG_IR24L : 0;
    buf += intStatus->drdy ? AK9750_INT_STATUS_FLAG_DRDY : 0;
    
    Status status;
    if ((status=write(AK9750_REG_ADDR_EINTEN, &buf, AK9750_LEN_ONE_BYTE)) != SUCCESS) {
        return status;    
    }
    
    char readback = 0;
    if ((status=read(AK9750_REG_ADDR_EINTEN, &readback, AK9750_LEN_ONE_BYTE)) != SUCCESS) {
        return status;
    }
    if ((readback & AK9750_INT_STATUS_MASK) != buf) {
        return ERROR;
    }
    
    return SUCCESS;
}

AK9750::Status AK9750::setInterruptEnableToEEPROM(const AK9750::InterruptStatus *intStatus) {
    char buf = 0;

    buf += intStatus->ir13h ? AK9750_INT_STATUS_FLAG_IR13H : 0;
    buf += intStatus->ir13l ? AK9750_INT_STATUS_FLAG_IR13L : 0;
    buf += intStatus->ir24h ? AK9750_INT_STATUS_FLAG_IR24H : 0;
    buf += intStatus->ir24l ? AK9750_INT_STATUS_FLAG_IR24L : 0;
    buf += intStatus->drdy ? AK9750_INT_STATUS_FLAG_DRDY : 0;
    
    Status status;
    char readback = 0;

    if (setEepromWriteMode() != SUCCESS)
    	return ERROR;

    if ((status=write(AK9750_EEPROM_ADDR_EINTEN, &buf, AK9750_LEN_ONE_BYTE)) != SUCCESS)
        return status;

    wait_ms(10);

    if ((status=read(AK9750_EEPROM_ADDR_EINTEN, &readback, AK9750_LEN_ONE_BYTE)) != SUCCESS)
        return status;

    if ((readback & AK9750_INT_STATUS_MASK) != buf)
        return ERROR;
    
    return SUCCESS;
}

AK9750::Status AK9750::setThreshold(const Threshold *th) {
    Status status;
    char buf[AK9750_LEN_BUF_THRESHOLD];
    
    buf[0] = (char)(((uint16_t)th->eth13h & 0x001F) << 3);   // ETH13HL
    buf[1] = (char)(((uint16_t)th->eth13h & 0x0FFF) >> 5);   // ETH13HH
    buf[2] = (char)(((uint16_t)th->eth13l & 0x001F) << 3);   // ETH13LL 
    buf[3] = (char)(((uint16_t)th->eth13l & 0x0FFF) >> 5);   // ETH13HH
    buf[4] = (char)(((uint16_t)th->eth24h & 0x001F) << 3);   // ETH24HL
    buf[5] = (char)(((uint16_t)th->eth24h & 0x0FFF) >> 5);   // ETH24HH
    buf[6] = (char)(((uint16_t)th->eth24l & 0x001F) << 3);   // ETH24LL 
    buf[7] = (char)(((uint16_t)th->eth24l & 0x0FFF) >> 5);   // ETH24HH
    
    if ((status=write(AK9750_REG_ADDR_ETH13HL, buf, AK9750_LEN_BUF_THRESHOLD)) != SUCCESS) {
        return status;
    }

    return SUCCESS;
}

AK9750::Status AK9750::setThresholdToEEPROM(const Threshold *th) {
    Status status;
    char buf[AK9750_LEN_BUF_THRESHOLD];
    
    buf[0] = (char)(((uint16_t)th->eth13h & 0x003F) );       // ETH13HL
    buf[1] = (char)(((uint16_t)th->eth13h & 0x0FFF) >> 6);   // ETH13HH
    buf[2] = (char)(((uint16_t)th->eth13l & 0x003F) );       // ETH13LL 
    buf[3] = (char)(((uint16_t)th->eth13l & 0x0FFF) >> 6);   // ETH13HH
    buf[4] = (char)(((uint16_t)th->eth24h & 0x003F) );       // ETH24HL
    buf[5] = (char)(((uint16_t)th->eth24h & 0x0FFF) >> 6);   // ETH24HH
    buf[6] = (char)(((uint16_t)th->eth24l & 0x003F) );       // ETH24LL 
    buf[7] = (char)(((uint16_t)th->eth24l & 0x0FFF) >> 6);   // ETH24HH
    
    for(int i = 0; i < AK9750_LEN_BUF_THRESHOLD; i++){
        if (setEepromWriteMode() != SUCCESS)
        	return ERROR;

        if ((status=write(AK9750_EEPROM_ADDR_ETH13HL+i, &buf[i], AK9750_LEN_ONE_BYTE)) != SUCCESS)
            return status;

        wait_ms(10);
    }

    return SUCCESS;
}
AK9750::Status AK9750::getThreshold(Threshold *th) {

	Status status;
    char buf[AK9750_LEN_BUF_THRESHOLD];
    
    if ((status=read(AK9750_REG_ADDR_ETH13HL, buf, AK9750_LEN_BUF_THRESHOLD)) != SUCCESS)
        return status;
    
    th->eth13h = ((int16_t)((uint16_t)buf[1] << 9 | buf[0] << 1) >> 4);
    th->eth13l = ((int16_t)((uint16_t)buf[3] << 9 | buf[2] << 1) >> 4);
    th->eth24h = ((int16_t)((uint16_t)buf[5] << 9 | buf[4] << 1) >> 4);
    th->eth24l = ((int16_t)((uint16_t)buf[7] << 9 | buf[6] << 1) >> 4);
    
    return SUCCESS;
}

AK9750::Status AK9750::getThresholdFromEEPROM(Threshold *th) {

	Status status;
    char buf[AK9750_LEN_BUF_THRESHOLD];

    if (setEepromReadMode() != SUCCESS)
    	return ERROR;

    if ((status=read(AK9750_EEPROM_ADDR_ETH13HL, buf, AK9750_LEN_BUF_THRESHOLD)) != SUCCESS)
        return status;
    
    th->eth13h = ((int16_t)((uint16_t)(buf[1]&0x3F) << 6 | (buf[0]&0x3F)));
    th->eth13l = ((int16_t)((uint16_t)(buf[3]&0x3F) << 6 | (buf[2]&0x3F)));
    th->eth24h = ((int16_t)((uint16_t)(buf[5]&0x3F) << 6 | (buf[4]&0x3F)));
    th->eth24l = ((int16_t)((uint16_t)(buf[7]&0x3F) << 6 | (buf[6]&0x3F)));
    
    return SUCCESS;
}
  
AK9750::Status AK9750::setHysteresis(const Hysteresis *hy) {

    Status status;
    char buf[AK9750_LEN_BUF_HYSTERESIS];

    // sanity check
    if (hy->ehys13 > AK9750_HYSTERESIS_MAX_VAL || hy->ehys24 > AK9750_HYSTERESIS_MAX_VAL) {
        return ERROR_ARG_OUT_OF_BOUNDS;
    }
    
    buf[0] = hy->ehys13;
    buf[1] = hy->ehys24;

    if ((status=write(AK9750_REG_ADDR_EHYS13, buf, AK9750_LEN_BUF_HYSTERESIS)) != SUCCESS)
        return status;
    
    return SUCCESS;
}

AK9750::Status AK9750::setHysteresisToEEPROM(const Hysteresis *hy) {

	Status status;
    char buf[AK9750_LEN_BUF_HYSTERESIS];

    // sanity check
    if (hy->ehys13 > AK9750_HYSTERESIS_MAX_VAL || hy->ehys24 > AK9750_HYSTERESIS_MAX_VAL) {
        return ERROR_ARG_OUT_OF_BOUNDS;
    }
    
    buf[0] = hy->ehys13;
    buf[1] = hy->ehys24;

    for(int i=0; i<AK9750_LEN_BUF_HYSTERESIS; i++){
        if (setEepromWriteMode() != SUCCESS)
        	return ERROR;

        if ((status=write(AK9750_EEPROM_ADDR_EHYS13+i, &buf[i], AK9750_LEN_ONE_BYTE)) != SUCCESS)
            return status;

        wait_ms(10);
    }
    
    return SUCCESS;
}

AK9750::Status AK9750::getHysteresis(Hysteresis *hy) {

	Status status;
    char buf[AK9750_LEN_BUF_HYSTERESIS];
    
    if ((status=read(AK9750_REG_ADDR_EHYS13, buf, AK9750_LEN_BUF_HYSTERESIS)) != SUCCESS)
        return status;
    
    hy->ehys13 = (buf[0] & AK9750_HYSTERESIS_MASK);
    hy->ehys24 = (buf[1] & AK9750_HYSTERESIS_MASK);
    
    return SUCCESS;
}

AK9750::Status AK9750::getHysteresisFromEEPROM(Hysteresis *hy) {

	Status status;
    char buf[AK9750_LEN_BUF_HYSTERESIS];

    if (setEepromReadMode() != SUCCESS)
    	return ERROR;

    if ((status=read(AK9750_EEPROM_ADDR_EHYS13, buf, AK9750_LEN_BUF_HYSTERESIS)) != SUCCESS)
        return status;
    
    hy->ehys13 = (buf[0] & AK9750_HYSTERESIS_MASK);
    hy->ehys24 = (buf[1] & AK9750_HYSTERESIS_MASK);
    
    return SUCCESS;
}

AK9750::Status AK9750::getOperationMode(OperationMode *mode, DigitalFilter *filter) {
    
	Status status;
    char val;

    if ((status=read(AK9750_REG_ADDR_ECNTL1, &val, AK9750_LEN_ONE_BYTE)) != SUCCESS)
        return status;

    *mode = AK9750::OperationMode(val & 0x07);
    *filter = AK9750::DigitalFilter((val & 0x38)>>3);
    
    return SUCCESS;
}

AK9750::Status AK9750::getOperationModeFromEEPROM(OperationMode *mode, DigitalFilter *filter) {

	Status status;
    char val;

	if (setEepromReadMode() != SUCCESS)
		return ERROR;

	if ((status=read(AK9750_EEPROM_ADDR_ECNTL1, &val, AK9750_LEN_ONE_BYTE)) != SUCCESS)
        return status;

    *mode = AK9750::OperationMode(val & 0x07);
    *filter = AK9750::DigitalFilter((val & 0x38)>>3);    

    return SUCCESS;
}

AK9750::Status AK9750::setOperationMode(OperationMode mode, DigitalFilter filter) {
    
	Status status;
    char val = (mode | (filter<<3));

    if ((status=write(AK9750_REG_ADDR_ECNTL1, &val, AK9750_LEN_ONE_BYTE)) != SUCCESS)
        return status;
    
    return SUCCESS;
}

AK9750::Status AK9750::setOperationModeToEEPROM(OperationMode mode, DigitalFilter filter) {

	Status status;
    char val = (mode | (filter<<3));

	if (setEepromWriteMode() != SUCCESS)
		return ERROR;

	if ((status=write(AK9750_EEPROM_ADDR_ECNTL1, &val, AK9750_LEN_ONE_BYTE)) != SUCCESS)
        return status;

    wait_ms(10);

    return SUCCESS;
}

AK9750::Status AK9750::reset() {

	Status status;
    char val = AK9750_VAL_SOFTWARE_RESET;

    if ((status=write(AK9750_REG_ADDR_ECNTL2, &val, AK9750_LEN_ONE_BYTE)) != SUCCESS)
        return status;
    
    return SUCCESS;
}

AK9750::Status AK9750::isDataReady() {
    
	Status status = AK9750::ERROR;
    char stValue[1];

    if ((status=AK9750::read(AK9750_REG_ADDR_ST1, stValue, 1)) != AK9750::SUCCESS)
        return status;							// I2C read failed.

    // Sets a return status corresponds to the obtained value.    
    if ((stValue[0] & AK9750_ST1_MASK_DRDY) > 0) {
        status = AK9750::DATA_READY;
    } else {
        status = AK9750::NOT_DATA_READY;
    }

    return status;
}

AK9750::Status AK9750::getSensorData(AK9750::SensorData *data) {

	Status status;
    char buf[AK9750_LEN_BUF_IR_DATA];
    
    if ((status=read(AK9750_REG_ADDR_INTST, buf, AK9750_LEN_BUF_IR_DATA)) != SUCCESS)
        return status;
    
    // DRDY check
    if( (buf[1] & AK9750_ST1_STATUS_FLAG_DRDY) == 0 )
        return ERROR;							// DRDY=0, data not ready
    
    // INTST status read
    data->intStatus.ir13h = ((buf[0] & AK9750_INT_STATUS_FLAG_IR13H) > 0) ? true : false;
    data->intStatus.ir13l = ((buf[0] & AK9750_INT_STATUS_FLAG_IR13L) > 0) ? true : false;
    data->intStatus.ir24h = ((buf[0] & AK9750_INT_STATUS_FLAG_IR24H) > 0) ? true : false;
    data->intStatus.ir24l = ((buf[0] & AK9750_INT_STATUS_FLAG_IR24L) > 0) ? true : false;
    data->intStatus.drdy = ((buf[0] & AK9750_INT_STATUS_FLAG_DRDY) > 0) ? true : false;
    
    // IR data
    data->ir1 = (int16_t)((buf[3] << 8) | buf[2]);
    data->ir2 = (int16_t)((buf[5] << 8) | buf[4]);
    data->ir3 = (int16_t)((buf[7] << 8) | buf[6]);
    data->ir4 = (int16_t)((buf[9] << 8) | buf[8]);

    // Temperature data
    data->temperature = (int16_t)((buf[11] << 8) | buf[10]);

    // DOR Status
    data->dor = ((buf[1] & AK9750_ST1_STATUS_FLAG_DOR) > 0) ? true : false;

    return SUCCESS;    
}
    
/**
 * set EEPROM write mode. 
 */
AK9750::Status AK9750::setEepromWriteMode(){

    Status status;
    char eeprom_mode = (char)AK9750::MODE_EEPROM_ACCESS;
    char ekey = (char)AK9750::EEPROM_WRITE_ENABLE;

    if ((status=write(AK9750_REG_ADDR_ECNTL1, &eeprom_mode, AK9750_LEN_ONE_BYTE)) != SUCCESS)
        return status;

    if ((status=write(AK9750_EEPROM_ADDR_EKEY, &ekey, AK9750_LEN_ONE_BYTE)) != SUCCESS)
        return status;    

    return SUCCESS;
}
    
/**
 * set EEPROM read mode. 
 */
AK9750::Status AK9750::setEepromReadMode(){

	Status status;
    char eeprom_mode = (char)AK9750::MODE_EEPROM_ACCESS;

    if ((status=write(AK9750_REG_ADDR_ECNTL1, &eeprom_mode, AK9750_LEN_ONE_BYTE)) != SUCCESS)
        return status;

    return SUCCESS;
}
