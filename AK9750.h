#ifndef __AK9750_H__
#define __AK9750_H__

#include "mbed.h"


/**
 * This is a device driver of AK9750/AK9753.
 *
 * @note AK9750 IR sensor device manufactured by AKM.
 * Example:
 * @code
 * #include "mbed.h"
 * #include "ak9750.h"
 * 
 * #define I2C_SPEED_100KHZ    100000
 * #define I2C_SPEED_400KHZ    400000
 * 
 * int main() {
 *     // Creates an instance of I2C
 *     I2C connection(I2C_SDA0, I2C_SCL0);
 *     connection.frequency(I2C_SPEED_100KHZ);
 *
 *     // TBD
 *
 *     while(true) {
 *              // TBD    
 *         }
 *     }
 * }
 * @endcode
*/
class AK9750
{
public:
    /**
     * Enum for return status.
     */
    typedef enum {
        SUCCESS,                 /**< Success */
        ERROR,                   /**< Error */
        ERROR_I2C_WRITE,         /**< I2C write error */
        ERROR_I2C_READ,          /**< I2C read error */
        ERROR_ARG_OUT_OF_BOUNDS, /**< An argument is out of bounds */
        DATA_READY,            /**< Data ready */
        NOT_DATA_READY,        /**< Data ready is not asserted. */
    } Status;
    
    /**
     * Enum for slave address of AK9750.
     */
    typedef enum {
        SLAVE_ADDR_1 = 0x64,   /**< CAD1=0, CAD0=0 */
        SLAVE_ADDR_2 = 0x65,   /**< CAD1=0, CAD0=1 */
        SLAVE_ADDR_3 = 0x66,   /**< CAD1=1, CAD0=0 */
    } SlaveAddress;
    
    /**
     * Enum for operation mode of measurement.
     */
    typedef enum {
        MODE_STANDBY = 0x00,        /**< Stand-by mode */
        MODE_EEPROM_ACCESS = 0x41,  /**< EEPROM access mode */
        MODE_SINGLE_SHOT = 0x02,    /**< Sigle shot mode */
        MODE_CONTINUOUS_0 = 0x04,   /**< Continuous measurement mode 0 */
        MODE_CONTINUOUS_1 = 0x05,   /**< Continuous measurement mode 1 */
        MODE_CONTINUOUS_2 = 0x06,   /**< Continuous measurement mode 2 */
        MODE_CONTINUOUS_3 = 0x07,   /**< Continuous measurement mode 3 */
    } OperationMode;

    /**
     * Enum for digital filter setting.
     */    
    typedef enum {
//        DF_0P3HZ = 0x00,         /**< Fc = 0.3 Hz */
//        DF_0P6HZ = 0x08,         /**< Fc = 0.6 Hz */
//        DF_1P1HZ = 0x10,         /**< Fc = 1.1 Hz */
//        DF_2P2HZ = 0x18,         /**< Fc = 2.2 Hz */
//        DF_4P4HZ = 0x20,         /**< Fc = 4.4 Hz */
//        DF_8P8HZ = 0x28,         /**< Fc = 8.8 Hz */
        DF_0P3HZ = 0x00,         /**< Fc = 0.3 Hz */
        DF_0P6HZ = 0x01,         /**< Fc = 0.6 Hz */
        DF_1P1HZ = 0x02,         /**< Fc = 1.1 Hz */
        DF_2P2HZ = 0x03,         /**< Fc = 2.2 Hz */
        DF_4P4HZ = 0x04,         /**< Fc = 4.4 Hz */
        DF_8P8HZ = 0x05,         /**< Fc = 8.8 Hz */
    } DigitalFilter;
    
    /**
     * Enum for EEPROM write enable setting.
     */    
    typedef enum {
        EEPROM_WRITE_DISABLE = 0x00,    /**< EEPROM write enable */
        EEPROM_WRITE_ENABLE =  0xA5,    /**< EEPROM write disable */
    } EepromWriteEnable;
    
    /**
     * Structure for intterupt status.
     */
    typedef struct {
        bool ir13h;             /**< IR13H interrupt */
        bool ir13l;             /**< IR13L interrupt */
        bool ir24h;             /**< IR24H interrupt */
        bool ir24l;             /**< IR24L interrupt */
        bool drdy;              /**< DRDY interrupt */
    } InterruptStatus;

    /**
     * Structure for threshold.
     */    
    typedef struct {
        int16_t eth13h;        /**< High Threshold level of sensor 1 and sensor 3. */
        int16_t eth13l;        /**< Low Threshold level of sensor 1 and sensor 3. */
        int16_t eth24h;        /**< High Threshold level of sensor 2 and sensor 4. */
        int16_t eth24l;        /**< Low Threshold level of sensor 2 and sensor 4. */
    } Threshold;

    /**
     * Structure for hysteresis.
     */
    typedef struct {
        uint8_t ehys13;         /**< Hysteresis of sensor 1 and sensor 3 */
        uint8_t ehys24;         /**< Hysteresis of sensor 2 and sensor 4 */
    } Hysteresis;
    
    /**
     * Structure for measurement data.
     */
    typedef struct {
        InterruptStatus intStatus;  /**< Interrupt status */
        int16_t ir1;                /**< IR1 sensor data */
        int16_t ir2;                /**< IR2 sensor data */
        int16_t ir3;                /**< IR3 sensor data */
        int16_t ir4;                /**< IR4 sensor data */
        int16_t temperature;        /**< temperature sensor data */
        bool dor;                   /**< Data over run status */
    } SensorData;
    
    /**
     * Constructor.
     *
     * @param conn instance of I2C
     * @param addr slave address of the device
     */
    AK9750();

    void init(I2C *conn, SlaveAddress addr);

    /**
     * Checks AK9750 connection.
     * @return Returns SUCCESS if connection is confirmed, otherwise returns other value.
     */
    Status checkConnection();
    
    /**
     * Gets interrupt enable/disable status.
     * @param intStatus interrupt status
     * @return SUCCESS if the interrupt status is obtained successfully, otherwise returns other value.
     */
    Status getInterruptEnable(InterruptStatus *intStatus);
    
    /**
     * Gets interrupt enable/disable status from EEPROM.
     * @param intStatus interrupt status
     * @return SUCCESS if the interrupt status is obtained successfully, otherwise returns other value.
     */
    Status getInterruptEnableFromEEPROM(InterruptStatus *intStatus);
    
    /**
     * Sets interrupt enable/disable status.
     * @param intStatus interrupt status
     * @return SUCCESS if the interrupt status is set successfully, otherwise returns other value.
     */
    Status setInterruptEnable(const InterruptStatus *intStatus);
    
    /**
     * Sets interrupt enable/disable status to EEPROM.
     * @param intStatus interrupt status
     * @return SUCCESS if the interrupt status is set successfully, otherwise returns other value.
     */
    Status setInterruptEnableToEEPROM(const InterruptStatus *intStatus);
    
    /**
     * Gets sensor operation mode.
     * @param mode Pointer to the operation mode.
     * @param filter Pointer to the digital filter setting.
     * @return SUCCESS if operation mode is set successfully, otherwise returns other value.
     */
    Status getOperationMode(OperationMode *mode, DigitalFilter *filter);
    
    /**
     * Gets sensor operation mode from EEPROM.
     * @param mode Pointer to the operation mode.
     * @param filter Pointer to the digital filter setting.
     * @return SUCCESS if operation mode is set successfully, otherwise returns other value.
     */
    Status getOperationModeFromEEPROM(OperationMode *mode, DigitalFilter *filter);
       
    /**
     * Sets sensor operation mode.
     * @param mode operation mode to be set.
     * @param filter digital filter setting to be set.
     * @return SUCCESS if operation mode is set successfully, otherwise returns other value.
     */
    Status setOperationMode(OperationMode mode, DigitalFilter filter = DF_0P3HZ);
       
    /**
     * Sets sensor operation mode to EEPROM.
     * @param mode operation mode to be set.
     * @param filter digital filter setting to be set.
     * @return SUCCESS if operation mode is set successfully, otherwise returns other value.
     */
    Status setOperationModeToEEPROM(OperationMode mode, DigitalFilter filter = DF_0P3HZ);
    
    /**
     * Sets threshold.
     * @param th Pointer to the threshold structure to be set.
     * @return SUCCESS if threshold is set successfully, otherwise returns other value.
     */
    Status setThreshold(const Threshold *th);
    
    /**
     * Sets threshold to EEPROM.
     * @param th Pointer to the threshold structure to be set.
     * @return SUCCESS if threshold is set successfully, otherwise returns other value.
     */
    Status setThresholdToEEPROM(const Threshold *th);
    
    /**
     * Gets threshold.
     * @param th Pointer to the threshold structure to store the read data.
     * @return SUCCESS if threshold is read successfully, otherwise returns other value.
     */
    Status getThreshold(Threshold *th);
    
    /**
     * Gets threshold from EEPROM.
     * @param th Pointer to the threshold structure to store the read data.
     * @return SUCCESS if threshold is read successfully, otherwise returns other value.
     */
    Status getThresholdFromEEPROM(Threshold *th);
    
    /**
     * Sets hysteresis.
     * @param hy Pointer to the hysteresis structure to be set.
     * @return SUCCESS if threshold is set successfully, otherwise returns other value.
     */
    Status setHysteresis(const Hysteresis *hy);
    
    /**
     * Sets hysteresis to EEPROM.
     * @param hy Pointer to the hysteresis structure to be set.
     * @return SUCCESS if threshold is set successfully, otherwise returns other value.
     */
    Status setHysteresisToEEPROM(const Hysteresis *hy);
    
    /**
     * Gets hysteresis.
     * @param hy Pointer to the hysteresis structure to store the read data.
     * @return SUCCESS if threshold is set successfully, otherwise returns other value.
     */
    Status getHysteresis(Hysteresis *hy);
    
    /**
     * Gets hysteresis from EEPROM.
     * @param hy Pointer to the hysteresis structure to store the read data.
     * @return SUCCESS if threshold is set successfully, otherwise returns other value.
     */
    Status getHysteresisFromEEPROM(Hysteresis *hy);

    /**
     * Resets the device.
     * @return SUCCESS if the device is reset successfully, otherwise returns other value.
     */
    Status reset();
    
    /**
     * Gets sensor data.
     * @param data Pointer to the SensorData structure object to store the read data.
     * @return SUCCESS if data is obtained successfully, otherwise returns other value.
     */
    Status getSensorData(SensorData *data);
    
    /**
     * Check if data is ready, i.e. measurement is finished.
     *
     * @return Returns DATA_READY if data is ready or NOT_DATA_READY if data is not ready. If error happens, returns another code.
     */
    Status isDataReady();

    /**
     * Reads register(s).
     * @param registerAddress Register address to be read.
     * @param buf Buffer to store the read data.
     * @param length Length in bytes to be read.
     * @return SUCCESS if data is read successfully, otherwise returns other value.
     */
    Status read(char registerAddress, char *buf, int length);
    
    /**
     * Writes data into register(s).
     * @param registerAddress Register address to be written.
     * @param buf Data to be written.
     * @param length Length in bytes to be written.
     * @return SUCCESS if data is written successfully, otherwise returns other value.
     */
    Status write(char registerAddress, const char *buf, int length);
    
private:
    I2C *connection;				/**< I2C connection inherited from akmsensormanager */
    SlaveAddress slaveAddress;		/**< Slave address */
    Status setEepromWriteMode();	/**< Enable writing to the EEPROM */
    Status setEepromReadMode();		/**< Enable reading from the EEPROM */
};

#endif // __AK9750_H__
